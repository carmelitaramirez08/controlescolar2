﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class RepartoManejador
    {
        private RepartoAccesoDatos _repartoAccesoDatos = new RepartoAccesoDatos();


        public void Guardar(Reparto reparto)
        {
            _repartoAccesoDatos.Guardar(reparto);
        }
        public void Eliminar(int idreparto)
        {
            _repartoAccesoDatos.Eliminar(idreparto);
        }

        public List<Reparto> GetRepartos(string filtro)
        {
            var listRepartos = _repartoAccesoDatos.GetRepartos(filtro);
            //Llenar lista
            return listRepartos;
        }

        public List<Reparto> GetMateriasList(String filtro, ComboBox cm)
        {
            var listMaterias = new List<Reparto>();
            listMaterias = _repartoAccesoDatos.GetMateriasList(filtro, cm);
            return listMaterias;

        }

        public DataSet getMateriasss(string filtro)
        {
            DataSet ds = _repartoAccesoDatos.getMateriasss(filtro);
            return ds;
        }

    }
}
