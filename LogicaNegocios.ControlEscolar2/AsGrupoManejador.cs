﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class AsGrupoManejador
    {
        private AsGrupoAccesoDatos _asigAccesoDatos = new AsGrupoAccesoDatos();
        private RepartoAccesoDatos _repartoAccesoDatos = new RepartoAccesoDatos();
        //private GrupoAccesoDatos _grupoAccesoDatos = new Grup

    
    public void Guardar(AsGrupo asGrupo)
    {
            _asigAccesoDatos.Guardar(asGrupo);
    }
    public void Eliminar(int idasig)
        {
            _asigAccesoDatos.Eliminar(idasig);
        }

        public List<AsGrupo> GetAsig(string filtro)
        {
            var listGrupos = _asigAccesoDatos.GetGrupos(filtro);
            //Llenar lista
            return listGrupos;
        }

        public List<AsGrupo> GetGruposList(ComboBox cm)
        {
            var listGrupos = new List<AsGrupo>();
            listGrupos = _asigAccesoDatos.GetGruposList(cm);     //_AsGrupoAccesoDatos.GetGruposList(cm);
            return listGrupos;
        }


        public List<AsGrupo> GetAlumnosList(String filtro, ComboBox cm)
        {
            var listAlumnos = new List<AsGrupo>();
            listAlumnos = _asigAccesoDatos.GetAlumnosList (filtro, cm);
            return listAlumnos;

        }

        public List<Reparto> GetMateriasList(String filtro, ComboBox cm)
        {
            var listMaterias = new List<Reparto>();
            listMaterias = _repartoAccesoDatos.GetMateriasList(filtro, cm);
            return listMaterias;

        }
    }
}
