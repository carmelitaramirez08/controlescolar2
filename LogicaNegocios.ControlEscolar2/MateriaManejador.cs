﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
   public class MateriaManejador
    {
        private MateriaAccesoDatos _materiaAccesoDatos = new MateriaAccesoDatos();



        public void Guardar(Materia materia)
        {
            _materiaAccesoDatos.Guardar(materia);
        }
        public void Eliminar(int idmateria)
        {
            _materiaAccesoDatos.Eliminar(idmateria);
        }


        public List<Materia> GetMaterias(string filtro)
        {
            var listMaterias = _materiaAccesoDatos.GetMaterias(filtro);
            //Llenar lista
            return listMaterias;
        }

        /*public List<Materia>GetMateriasList(string filtro)
        {
            var listMaterias = _materiaAccesoDatos.GetMateriasList(filtro);
            return listMaterias;
        }
        */

        public List<Materia> GetMateriasList(ComboBox cm)
        {
            var listMaterias = new List<Materia>();
            listMaterias = _materiaAccesoDatos.GetMateriasList(cm);
            return listMaterias;
        }
        public DataTable clave(string filtro)
        {
            var dtt = _materiaAccesoDatos.idmateria(filtro);
            return dtt;
        }

    }
}
