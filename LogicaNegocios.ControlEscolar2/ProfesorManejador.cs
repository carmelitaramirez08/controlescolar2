﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace LogicaNegocios.ControlEscolar2
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();
        private EstudiosAccesoDatos _estudioAccesoDatos = new EstudiosAccesoDatos();

        public void Guardar(Profesores profesor)
        {
            _profesorAccesoDatos.Guardar(profesor);
        }
        public void Eliminar(int ID)
        {
            _profesorAccesoDatos.Eliminar( ID);
        }

        public List<Profesores> GetProfesores(string filtro)
        {
            var listProfesores = _profesorAccesoDatos.GetProfesores(filtro);
            //Llenar lista
            return listProfesores;
        }
        /*public DataSet LastID(string filtro)
        {
            var ds = _profesorAccesoDatos.LastID(filtro); 

            return ds;
        }
        */
        public DataTable idprofesor(string filtro)
        {
            var dtt = _profesorAccesoDatos.idprofesor(filtro);
            return dtt;
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            var listEstudios = _estudioAccesoDatos.GetEstudios(filtro);
            //Llenar lista
            return listEstudios;
        }


        public List<Profesores> GetProfesoresList(ComboBox cm)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetProfesoresList (cm);
            return listProfesores;
        }

        public List<Profesores> GetNumero(ComboBox cm)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetProfesoresNumeroControl(cm);
            return listProfesores;
        }

        public List<Profesores> GetID(string filtro)
        {
            var listProfesores = new List<Profesores>();
            listProfesores = _profesorAccesoDatos.GetID(filtro);
            return listProfesores;
        }

        
    }



}
