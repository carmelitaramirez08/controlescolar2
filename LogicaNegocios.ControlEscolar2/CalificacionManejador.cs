﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace LogicaNegocios.ControlEscolar2
{
    public class CalificacionManejador
    {
        private CalificacionAccesoDatos _calificacionAccesoDatos = new CalificacionAccesoDatos();

        public void Guardar(Calificacion calificacion)
        {
            _calificacionAccesoDatos.Guardar(calificacion);
        }
        public void Eliminar(int idcalificacion)
        {
            _calificacionAccesoDatos.Eliminar(idcalificacion);
        }

        public List<Calificacion> GetCalificaciones(string filtro)
        {
            var listCalificaciones = _calificacionAccesoDatos.GetCalificaciones(filtro);
            //Llenar lista
            return listCalificaciones;
        }

    }
}
