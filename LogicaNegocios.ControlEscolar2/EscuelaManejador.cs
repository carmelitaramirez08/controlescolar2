﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
   public class EscuelaManejador
    {

        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();
 

        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);
        }
        public void Eliminar(int idescuela)
        {
            _escuelaAccesoDatos.Eliminar(idescuela);
        }

        public List<Escuela> GetEscuela(string filtro)
        {
            var listEscuela = _escuelaAccesoDatos.GetEscuelas(filtro);
            //Llenar lista
            return listEscuela;
        }
       
        
        public List<Escuela> GetEscuelas(string filtro)
        {
            var listEscuela = _escuelaAccesoDatos.GetEscuelas(filtro);
            //Llenar lista
            return listEscuela;
        }

        public DataSet getDatos()
        {
            DataSet ds = _escuelaAccesoDatos.getDatos();
            return ds;
        }
    }
}
