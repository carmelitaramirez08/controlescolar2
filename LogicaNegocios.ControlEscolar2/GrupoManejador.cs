﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace LogicaNegocios.ControlEscolar2
{
    public class GrupoManejador
    {
        private GrupoAccesoDatos _grupoAccesoDatos = new GrupoAccesoDatos();

        public void Guardar(Grupo grupo)
        {
            _grupoAccesoDatos.Guardar(grupo);
        }
        public void Eliminar(int idgrupo)
        {
            _grupoAccesoDatos.Eliminar(idgrupo);
        }

        public List<Grupo> GetGrupos(string filtro)
        {
            var listGrupos = _grupoAccesoDatos.GetGrupos(filtro);
            //Llenar lista
            return listGrupos;
        }
        public List<Grupo> GetGruposList(ComboBox cm)
        {
            var listGrupos = new List<Grupo>();
            listGrupos = _grupoAccesoDatos.GetGruposList(cm);
            return listGrupos;
        }

        public DataTable grupos(string filtro)
        {
            var dtt = _grupoAccesoDatos.grupos(filtro); 
            return dtt;
        }
    }
}
