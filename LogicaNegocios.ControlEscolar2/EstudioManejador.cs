﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;


namespace LogicaNegocios.ControlEscolar2
{
    public class EstudioManejador
    {
        private EstudiosAccesoDatos _estudioAccesoDatos = new EstudiosAccesoDatos();


    

    public void Guardar(Estudios estudio)
    {
        _estudioAccesoDatos.Guardar(estudio);
    }

    public void Eliminar(int idestudio)
    {
        _estudioAccesoDatos.Eliminar(idestudio);
    }

    public List<Estudios> GetEstudios(string filtro)
    {
        var listestudio = _estudioAccesoDatos.GetEstudios(filtro);
        return listestudio;
    }

        public List<Estudios> GetEstudios2(string filtro)
        {
            var listestudio = _estudioAccesoDatos.GetEstudios2(filtro);
            return listestudio;
        }

    }
}



