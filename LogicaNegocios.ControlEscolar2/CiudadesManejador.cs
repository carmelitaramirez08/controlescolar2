﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Windows.Forms;

namespace LogicaNegocios.ControlEscolar2
{
    public class CiudadesManejador
    {
        private CiudadesAccesoDatos _ciudadesAccesoDatos = new CiudadesAccesoDatos();

        public List<Ciudades> GetCiudadesList(String filtro, ComboBox cm)
        {
            var listCiudades = new List<Ciudades>();
            listCiudades = _ciudadesAccesoDatos.GetCiudadesList(filtro,cm);
            return listCiudades;

        }
    }
}
