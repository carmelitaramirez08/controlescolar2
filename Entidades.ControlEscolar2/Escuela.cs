﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
  public class Escuela
    {
        private int _idescuela;
        private string _nombre;
        private string _rfc;
        private string _domicilio;
        private string _telefono;
        private string _email;
        private string _paginaweb;
        private string _director;
        private string _logo;

        public int idescuela { get => _idescuela; set => _idescuela = value; }
        public string rfc { get => _rfc; set => _rfc = value; }
        public string nombre { get => _nombre; set => _nombre = value; }
        public string domicilio { get => _domicilio; set => _domicilio = value; }
        public string telefono { get => _telefono; set => _telefono = value; }
        public string email { get => _email; set => _email = value; }
        public string paginaweb { get => _paginaweb; set => _paginaweb = value; }
        public string director { get => _director; set => _director = value; }
        public string logo { get => _logo; set => _logo = value; }
    }
}

