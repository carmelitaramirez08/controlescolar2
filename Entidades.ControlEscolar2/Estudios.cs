﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estudios
    {
        private int _idestudio;
        private string _estudio;
        private int _anioestudio;
        private string _documento;
        private int _fk_profesor;
        

        public int idestudio { get => _idestudio; set => _idestudio = value; }
        public string estudio { get => _estudio; set => _estudio = value; }
        public int anioestudio { get => _anioestudio; set => _anioestudio = value; }
        public string documento { get => _documento; set => _documento = value; }
        public int fk_profesor { get => _fk_profesor; set => _fk_profesor = value; }
        
    }
}
