﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Profesores
    {
        private int _ID;
       
        private string _ncontrolm;
        private string _nombre;
        private string _apellidopaterno;
        private string _apellidomaterno;
        private string _direccion;
        private string _ciudad;
        private string _estado;
        private string _ncedula;
        private string _titulo;
        private string _fechanacimiento;

        public int ID { get => _ID; set => _ID = value; }
       

        public string ncontrolm { get => _ncontrolm; set => _ncontrolm = value; }
        public string nombre { get => _nombre; set => _nombre = value; }
        public string apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
        public string direccion { get => _direccion; set => _direccion = value; }
        public string ciudad { get => _ciudad; set => _ciudad = value; }
        public string estado { get => _estado; set => _estado = value; }
        public string ncedula { get => _ncedula; set => _ncedula = value; }
        public string titulo { get => _titulo; set => _titulo = value; }
        public string fechanacimiento { get => _fechanacimiento; set => _fechanacimiento = value; }
    }
}
