﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupo
    {
        private int _idgrupo;
        private string _nombre;
        //private string _descripcion;

        public int idgrupo { get => _idgrupo; set => _idgrupo = value; }
        public string nombre { get => _nombre; set => _nombre = value; }
       // public string descripcion { get => _descripcion; set => _descripcion = value; }
    }
}
