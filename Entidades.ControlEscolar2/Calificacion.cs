﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Calificacion
    {
        private int _idcalificacion;
        private string _alumno;

        private string _grupo;
        private string _materia;
        private int _uno;
        private int _dos;
        private int _tres;
        private int _cuatro;

        public int idcalificacion { get => _idcalificacion; set => _idcalificacion = value; }
        public string alumno { get => _alumno; set => _alumno = value; }
        
        public string grupo { get => _grupo; set => _grupo = value; }
        public string materia { get => _materia; set => _materia = value; }
        public int uno { get => _uno; set => _uno = value; }
        public int dos { get => _dos; set => _dos = value; }
        public int tres { get => _tres; set => _tres = value; }
        public int cuatro { get => _cuatro; set => _cuatro = value; }
    }

;
}
