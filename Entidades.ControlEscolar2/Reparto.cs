﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Reparto
    {
        private int _idreparto;
        private string _grupo;
        private string _fkprofesor;
        private string _materia;

        public int idreparto { get => _idreparto; set => _idreparto = value; }
        public string grupo { get => _grupo; set => _grupo = value; }
        public string fkprofesor { get => _fkprofesor; set => _fkprofesor = value; }
        public string materia { get => _materia; set => _materia = value; }
    }
}
