﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Ciudades
    {
        private string _fkcodigociudad;
        private string _nombreciu;
        private string _fkestado;

        public string Fkcodigociudad { get => _fkcodigociudad; set => _fkcodigociudad = value; }
        public string Nombreciu { get => _nombreciu; set => _nombreciu = value; }
        public string Fkestado { get => _fkestado; set => _fkestado = value; }

    }

}
