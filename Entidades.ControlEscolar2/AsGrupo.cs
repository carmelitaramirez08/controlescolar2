﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class AsGrupo
    {
        private int _idasig;
        private string _grupo;
        private int _alumno;
        private string _nombre;
       

        public int idasig { get => _idasig; set => _idasig = value; }
        public string grupo { get => _grupo; set => _grupo = value; }
        public int alumno { get => _alumno; set => _alumno = value; }
        public string nombre{ get => _nombre; set => _nombre = value; }

    }
}

