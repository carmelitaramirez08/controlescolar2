﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Materia
    {

        private int _idmateria;
        private string _clave;
        private string _nombre;
        private int _horast;
        private int _horasp;
        private int _creditos;
        private int _semestre;
        private string _anterior;
        private string _siguiente;

        public int idmateria { get => _idmateria; set => _idmateria = value; }
        public string clave { get => _clave; set => _clave = value; }
        public string nombre { get => _nombre; set => _nombre = value; }
        public int horast { get => _horast; set => _horast = value; }
        public int horasp { get => _horasp; set => _horasp = value; }
        public int creditos { get => _creditos; set => _creditos = value; }
        public int semestre { get => _semestre; set => _semestre = value; }
        public string anterior { get => _anterior; set => _anterior = value; }
        public string siguiente { get => _siguiente; set => _siguiente = value; }
    }
}
