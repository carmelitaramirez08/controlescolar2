﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estados
    {
        private string _fkcodigoestado;
        private string _nombre;


        public string Fkcodigoestado { get => _fkcodigoestado; set => _fkcodigoestado = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }

    }
}
