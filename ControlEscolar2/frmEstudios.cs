﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using System.IO;


namespace ControlEscolar2
{
    public partial class frmEstudios : Form
    {
        private EstudioManejador _estudiomanejador;
        private Estudios _estudio;

        private ProfesorManejador _profesormanejador;
        private Profesores _profesor;

        //PDF
        private OpenFileDialog _documentoPdf;
        private string _rutaP;


        public frmEstudios()
        {
            InitializeComponent();
            _estudiomanejador = new EstudioManejador();
            _estudio = new Estudios();

            _profesormanejador = new ProfesorManejador();
            _profesor = new Profesores();

            //PDF
            _documentoPdf = new OpenFileDialog();
            _rutaP = Application.StartupPath + "\\PDF\\";
        }

        private void FrmEstudios_Load(object sender, EventArgs e)
        {
            ProfesorManejador _profesormanejador = new ProfesorManejador();
            _profesormanejador.GetProfesoresList(cmbProfesor);

            BuscarEstudios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();


        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtEstudio.Enabled = activar;
            cmbProfesor.Enabled = activar;
            txtAño.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtEstudio.Text = "";
            
            lblID.Text = "0";
            cmbProfesor.Text = "";
            txtDocumento.Text = "";
          

        }
        private void BuscarEstudios(string filtro)
        {
            dgvEstudios.DataSource = _estudiomanejador.GetEstudios(filtro);
        }
        private void CargarEstudio()
        {
            _estudio.idestudio = Convert.ToInt32(lblID.Text);
            _estudio.estudio = txtEstudio.Text;
            _estudio.anioestudio = Convert.ToInt32(txtAño.Text);
            _estudio.documento = txtDocumento.Text;
            _estudio.fk_profesor = Convert.ToInt32(cmbProfesor.Text);
            
        }

        private void CmbProfesor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProfesorManejador _profesormanejador = new ProfesorManejador();
            _profesormanejador.GetID(cmbProfesor.Text);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtEstudio.Focus();
        }
        private void GuardarEstudio()
        {
            _estudiomanejador.Guardar(_estudio);

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            
            CargarEstudio();
            if (true)
            {
                try
                {
                    GuardarEstudio();
                    LimpiarCuadros();
                    BuscarEstudios("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GuardarPDF()
        {

            if (_documentoPdf.FileName != null)
            {
                if (_documentoPdf.FileName != "")
                {
                    var archivo = new FileInfo(_documentoPdf.FileName);
                    if (Directory.Exists(_rutaP))
                    {
                        //Codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_rutaP, ".pdf");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            //Codigo para reemplazar imagen

                            archivoAnterior = new FileInfo(obtenerArchivos[0]);

                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_rutaP + archivo.Name);
                            }
                        }
                        else
                        {
                            try
                            {
                                archivo.CopyTo(_rutaP + archivo.Name);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                               
                            }
                            
                        }


                    }
                    else

                    {
                        Directory.CreateDirectory(_rutaP);
                        archivo.CopyTo(_rutaP + archivo.Name);
                    }
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudios(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarEstudio();
                    BuscarEstudios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
      
        private void EliminarEstudio()
        {
            var idestudio = dgvEstudios.CurrentRow.Cells["idestudio"].Value;
            _estudiomanejador.Eliminar(Convert.ToInt32(idestudio));
        }
        private void ModificarEstudio()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblID.Text = dgvEstudios.CurrentRow.Cells["idestudio"].Value.ToString();
            txtEstudio.Text = dgvEstudios.CurrentRow.Cells["estudio"].Value.ToString();
            txtAño.Text = dgvEstudios.CurrentRow.Cells["anioestudio"].Value.ToString();
            txtDocumento.Text = dgvEstudios.CurrentRow.Cells["documento"].Value.ToString();

            cmbProfesor.Text = dgvEstudios.CurrentRow.Cells["fk_profesor"].Value.ToString();
        }

        private void DgvEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarEstudio();
            BuscarEstudios("");
        }

        private void BtnCargarPDF_Click(object sender, EventArgs e)
        {
            CargarPDF();
        }

        private void CargarPDF()
        {
            _documentoPdf.Filter = "Documento tipo (*.Pdf)|*.Pdf";
            _documentoPdf.Title = "Cargar PDF";
            _documentoPdf.ShowDialog();

            if (_documentoPdf.FileName != "")
            {
                var archivo = new FileInfo(_documentoPdf.FileName);

                txtDocumento.Text = archivo.Name;
            }
        }

        private void BtnSubir_Click(object sender, EventArgs e)
        {
            GuardarPDF();
        }

        

        private void EliminarPdf()
        {

            if (MessageBox.Show("Estas seguro de eliminar el documento", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)

                if (Directory.Exists(_rutaP))
                {
                    //Codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_rutaP, "*.pdf");
                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {
                        //Codigo para reemplazar imagen

                        archivoAnterior = new FileInfo(obtenerArchivos[0]);

                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }

                    }
                }

        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            EliminarPdf();
        }
    }
}
