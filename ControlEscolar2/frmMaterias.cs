﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmMaterias : Form
    {
        private MateriaManejador _materiaManejador;
        private Materia _materia;
        public frmMaterias()
        {
            InitializeComponent();
            _materiaManejador = new MateriaManejador();
            _materia = new Materia();
        }
        private void GuardarMateria()
        {
            _materiaManejador.Guardar(_materia);

        }
        public void getID(string filtro)
        {
            cmbID.DataSource = _materiaManejador.clave(filtro);
            cmbID.DisplayMember = "Nextincremento";
            if (cmbID.Text == "")
            {
                lblID2.Text = "M" + txtClave.Text + 1;
            }
            else
            {
                lblID2.Text = "M" +txtClave.Text + cmbID.Text;
            }


        }
        private void CargarMateria()
        {
            _materia.idmateria = Convert.ToInt32(lblID.Text);

            _materia.clave = txtClave.Text;
            _materia.nombre = txtNombre.Text;
            _materia.horast = Convert.ToInt32(txtHorasT.Text);
            _materia.horasp = Convert.ToInt32(txtHorasP.Text);
            _materia.creditos = Convert.ToInt32(txtCreditos.Text);
            _materia.semestre = Convert.ToInt32(cmbSemestre.Text);
            _materia.anterior = cmbAnterior.Text;
            _materia.siguiente = txtSiguiente.Text;


        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtClave.Enabled = activar;
            txtNombre.Enabled = activar;
            txtHorasT.Enabled = activar;
            txtHorasP.Enabled = activar;

            txtCreditos.Enabled = activar;
            cmbSemestre.Enabled = activar;
            cmbAnterior.Enabled = activar;
            txtSiguiente.Enabled = activar;
            
            dgvMaterias.Enabled = activar;
        }
        private void Limpiar()
        {

            txtClave.Clear();
            txtNombre.Clear();
            txtHorasT.Clear();
            txtHorasP.Clear();

            txtCreditos.Clear();
            cmbSemestre.Text = "";
            
            txtSiguiente.Clear();

            lblID.Text = "0";
        }
        private void BuscarProfesores(string filtro)
        {
            dgvMaterias.DataSource = _materiaManejador.GetMaterias(filtro);
        }
        private void FrmMaterias_Load(object sender, EventArgs e)
        {
            MateriaManejador _materiaManejador = new MateriaManejador();
            _materiaManejador.GetMateriasList(cmbAnterior);
        }

        private void CmbSemestre_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }
        private void EliminarMateria()
        {
            var idmateria = dgvMaterias.CurrentRow.Cells["idmateria"].Value;
            _materiaManejador.Eliminar(Convert.ToInt32(idmateria));
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaterias(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            Limpiar();
            
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarMateria();
                    BuscarProfesores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }



        private void ModificarMaterias()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblID.Text = dgvMaterias.CurrentRow.Cells["idmateria"].Value.ToString();
           
            txtNombre.Text = dgvMaterias.CurrentRow.Cells["nombre"].Value.ToString();
            txtHorasT.Text = dgvMaterias.CurrentRow.Cells["horast"].Value.ToString();
            txtHorasP.Text = dgvMaterias.CurrentRow.Cells["horasp"].Value.ToString();
            txtCreditos.Text = dgvMaterias.CurrentRow.Cells["creditos"].Value.ToString();
            cmbSemestre.Text = dgvMaterias.CurrentRow.Cells["semestre"].Value.ToString();
            
            cmbAnterior.Text = dgvMaterias.CurrentRow.Cells["anterior"].Value.ToString();
            txtSiguiente.Text = dgvMaterias.CurrentRow.Cells["siguiente"].Value.ToString();
        }

        private void DgvMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarMaterias();
            BuscarMaterias("");
            
            
        }
        public void creditos()
        {
            
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarMateria();
            if (true)
            {
                try
                {
                    GuardarMateria();
                    Limpiar();
                    BuscarMaterias("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void BuscarMaterias(string filtro)
        {
            dgvMaterias.DataSource = _materiaManejador.GetMaterias(filtro);
        }
        private void TxtClave_TextChanged(object sender, EventArgs e)
        {
            getID(txtClave.Text);
        }

        private void CmbAnterior_SelectedIndexChanged(object sender, EventArgs e)
        {
            MateriaManejador _materiaManejador = new MateriaManejador();
            _materiaManejador.GetMateriasList(cmbAnterior);
        }
    }
}
