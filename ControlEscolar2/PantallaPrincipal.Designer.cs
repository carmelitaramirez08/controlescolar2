﻿namespace ControlEscolar2
{
    partial class PantallaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaPrincipal));
            this.msMenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.cATALOGOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escuelaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gruposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesGruposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesProfesorYMateriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.msMenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMenuPrincipal
            // 
            this.msMenuPrincipal.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.msMenuPrincipal.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.msMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cATALOGOSToolStripMenuItem});
            this.msMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.msMenuPrincipal.Name = "msMenuPrincipal";
            this.msMenuPrincipal.Size = new System.Drawing.Size(1600, 42);
            this.msMenuPrincipal.TabIndex = 0;
            this.msMenuPrincipal.Text = "menuStrip1";
            // 
            // cATALOGOSToolStripMenuItem
            // 
            this.cATALOGOSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.alumnosToolStripMenuItem,
            this.profesoresToolStripMenuItem,
            this.estudiosToolStripMenuItem,
            this.escuelaToolStripMenuItem,
            this.materiasToolStripMenuItem,
            this.gruposToolStripMenuItem,
            this.asignacionesGruposToolStripMenuItem,
            this.asignacionesProfesorYMateriasToolStripMenuItem,
            this.calificacionesToolStripMenuItem});
            this.cATALOGOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cATALOGOSToolStripMenuItem.Image")));
            this.cATALOGOSToolStripMenuItem.Name = "cATALOGOSToolStripMenuItem";
            this.cATALOGOSToolStripMenuItem.Size = new System.Drawing.Size(183, 38);
            this.cATALOGOSToolStripMenuItem.Text = "CATALOGO";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("usuariosToolStripMenuItem.Image")));
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.UsuariosToolStripMenuItem_Click);
            // 
            // alumnosToolStripMenuItem
            // 
            this.alumnosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("alumnosToolStripMenuItem.Image")));
            this.alumnosToolStripMenuItem.Name = "alumnosToolStripMenuItem";
            this.alumnosToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.alumnosToolStripMenuItem.Text = "Alumnos";
            this.alumnosToolStripMenuItem.Click += new System.EventHandler(this.AlumnosToolStripMenuItem_Click);
            // 
            // profesoresToolStripMenuItem
            // 
            this.profesoresToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources.descarga;
            this.profesoresToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.profesoresToolStripMenuItem.Name = "profesoresToolStripMenuItem";
            this.profesoresToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.profesoresToolStripMenuItem.Text = "Profesores";
            this.profesoresToolStripMenuItem.Click += new System.EventHandler(this.ProfesoresToolStripMenuItem_Click);
            // 
            // estudiosToolStripMenuItem
            // 
            this.estudiosToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources.depositphotos_76335177_stock_illustration_female_student_icon;
            this.estudiosToolStripMenuItem.Name = "estudiosToolStripMenuItem";
            this.estudiosToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.estudiosToolStripMenuItem.Text = "Estudios";
            this.estudiosToolStripMenuItem.Click += new System.EventHandler(this.EstudiosToolStripMenuItem_Click);
            // 
            // escuelaToolStripMenuItem
            // 
            this.escuelaToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources.pngtree_school_icon_png_image_1606554;
            this.escuelaToolStripMenuItem.Name = "escuelaToolStripMenuItem";
            this.escuelaToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.escuelaToolStripMenuItem.Text = "Escuela";
            this.escuelaToolStripMenuItem.Click += new System.EventHandler(this.EscuelaToolStripMenuItem_Click);
            // 
            // materiasToolStripMenuItem
            // 
            this.materiasToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources._43810;
            this.materiasToolStripMenuItem.Name = "materiasToolStripMenuItem";
            this.materiasToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.materiasToolStripMenuItem.Text = "Materias";
            this.materiasToolStripMenuItem.Click += new System.EventHandler(this.MateriasToolStripMenuItem_Click);
            // 
            // gruposToolStripMenuItem
            // 
            this.gruposToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources._32441;
            this.gruposToolStripMenuItem.Name = "gruposToolStripMenuItem";
            this.gruposToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.gruposToolStripMenuItem.Text = "Grupos";
            this.gruposToolStripMenuItem.Click += new System.EventHandler(this.GruposToolStripMenuItem_Click);
            // 
            // asignacionesGruposToolStripMenuItem
            // 
            this.asignacionesGruposToolStripMenuItem.BackgroundImage = global::ControlEscolar2.Properties.Resources._66128_web_widget_form_icons_computer_social_browser1;
            this.asignacionesGruposToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources._66128_web_widget_form_icons_computer_social_browser2;
            this.asignacionesGruposToolStripMenuItem.Name = "asignacionesGruposToolStripMenuItem";
            this.asignacionesGruposToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.asignacionesGruposToolStripMenuItem.Text = "Asignaciones Grupos";
            this.asignacionesGruposToolStripMenuItem.Click += new System.EventHandler(this.AsignacionesGruposToolStripMenuItem_Click);
            // 
            // asignacionesProfesorYMateriasToolStripMenuItem
            // 
            this.asignacionesProfesorYMateriasToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources.asignacion_delegar_delegar_distribucion_de_colores_planos_icono_icono_de_vector_plantilla_de_titulo_t9f207;
            this.asignacionesProfesorYMateriasToolStripMenuItem.Name = "asignacionesProfesorYMateriasToolStripMenuItem";
            this.asignacionesProfesorYMateriasToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.asignacionesProfesorYMateriasToolStripMenuItem.Text = "Asignaciones Profesor y materias";
            this.asignacionesProfesorYMateriasToolStripMenuItem.Click += new System.EventHandler(this.AsignacionesProfesorYMateriasToolStripMenuItem_Click);
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.Image = global::ControlEscolar2.Properties.Resources.descarga__2_;
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(499, 44);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            this.calificacionesToolStripMenuItem.Click += new System.EventHandler(this.CalificacionesToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(597, 768);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(390, 42);
            this.label1.TabIndex = 1;
            this.label1.Text = "CONTROL ESCOLAR";
            // 
            // PantallaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ControlEscolar2.Properties.Resources.maxresdefault;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1600, 865);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msMenuPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMenuPrincipal;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "PantallaPrincipal";
            this.Text = "CONTROL ESCOLAR";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.msMenuPrincipal.ResumeLayout(false);
            this.msMenuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem cATALOGOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudiosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escuelaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materiasToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem gruposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionesGruposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionesProfesorYMateriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
    }
}