﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using System.IO;

namespace ControlEscolar2
{
    public partial class frmReparto : Form
    {

        private ProfesorManejador _profesormanejador;
        private Profesores _profesor;

        private MateriaManejador _materiamanejador;
        private Materia _materia;

        private GrupoManejador _grupomanejador;
        private Grupo _grupo;

        private RepartoManejador _repartomanejador;
        private Reparto _reparto;
        public frmReparto()
        {
            InitializeComponent();
            _profesormanejador = new ProfesorManejador();
            _profesor = new Profesores();

            _materiamanejador = new MateriaManejador();
            _materia = new Materia();

            _grupomanejador = new GrupoManejador();
            _grupo = new Grupo();

            _repartomanejador = new RepartoManejador();
            _reparto = new Reparto();
        }

        private void FrmReparto_Load(object sender, EventArgs e)
        {
          
            _profesormanejador.GetNumero(cmbProfesor);
            _grupomanejador.GetGruposList(cmbGrupo);
            _materiamanejador.GetMateriasList(cmbMateria);

            BuscarRepartos("");
            ControlarBotones(true, false, false, true);
           // ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        
        private void LimpiarCuadros()
        {
          
            cmbProfesor.Text = "";
            cmbMateria.Text = "";
            cmbGrupo.Text = "";


        }
        private void BuscarRepartos(string filtro)
        {
            dgvReparto.DataSource = _repartomanejador.GetRepartos(filtro);
        }
        private void CargarReparto()
        {
            _reparto.idreparto = Convert.ToInt32(lblID.Text);
            _reparto.grupo = cmbGrupo.Text;
            _reparto.fkprofesor = cmbProfesor.Text;
            _reparto.materia = cmbMateria.Text;
           

        }
        private void GuardarReparto()
        {
            _repartomanejador.Guardar(_reparto);

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            //ControlarCuadros(true);

            
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarReparto();
            if (true)
            {
                try
                {
                    GuardarReparto();
                    LimpiarCuadros();
                    BuscarRepartos("");
                    ControlarBotones(true, false, false, true);
                    //ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarRepartos(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarReparto();
                    BuscarRepartos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarReparto()
        {
            var idreparto = dgvReparto.CurrentRow.Cells["idreparto"].Value;
            _repartomanejador.Eliminar(Convert.ToInt32(idreparto));
        }


        private void ModificarReparto()
        {
            // ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblID.Text = dgvReparto.CurrentRow.Cells["idreparto"].Value.ToString();
            cmbMateria.Text = dgvReparto.CurrentRow.Cells["materia"].Value.ToString();
            cmbProfesor.Text = dgvReparto.CurrentRow.Cells["fkprofesor"].Value.ToString();
            cmbGrupo.Text = dgvReparto.CurrentRow.Cells["grupo"].Value.ToString();
       
    }

        private void DgvReparto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarReparto();
            BuscarRepartos("");
        }
    }
}
