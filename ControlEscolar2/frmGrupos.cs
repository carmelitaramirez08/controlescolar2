﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Exportar_PDF_Excel;
using System.Drawing.Printing;


namespace ControlEscolar2
{
    public partial class frmGrupos : Form
    {

        private string _ruta;
        private GrupoManejador _grupomanejador;
        private Grupo _grupo;
        public frmGrupos()
        {
            InitializeComponent();
            _grupomanejador = new GrupoManejador();
            _grupo = new Grupo();

            _ruta = Application.StartupPath + "\\PDF\\";

        }

        private void FrmGrupos_Load(object sender, EventArgs e)
        {
            BuscarGrupos("");
            ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            
            LimpiarCuadros();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }

        private void LimpiarCuadros()
        {
            lblNombre.Text = "";

            lblID.Text = "0";




        }
        private void BuscarGrupos(string filtro)
        {
            dgvGrupos.DataSource = _grupomanejador.GetGrupos(filtro);
        }
        private void NAME()
        {
            lblNombre.Text = cmbSemestre.Text + " " + cmbGrupo.Text + " " + cmbCarrera.Text;
           }
        private void CargarGrupo()
        {
            NAME();
            _grupo.idgrupo = Convert.ToInt32(lblID.Text);
            NAME();
            _grupo.nombre = lblNombre.Text;
           
           

        }
        private void GuardarGrupo()
        {
            _grupomanejador.Guardar(_grupo);

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            //ControlarCuadros(true);

            //txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            {

                CargarGrupo();
                if (true)
                {
                    try
                    {
                        GuardarGrupo();
                        LimpiarCuadros();
                        BuscarGrupos("");
                        ControlarBotones(true, false, false, true);
                        //ControlarCuadros(false);
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
    }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupos(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarGrupo();
                    BuscarGrupos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarGrupo()
        {
            var idgrupo = dgvGrupos.CurrentRow.Cells["idgrupo"].Value;
            _grupomanejador.Eliminar(Convert.ToInt32(idgrupo));
        }

        private void ModificarGrupo()
        {
            //ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblID.Text = dgvGrupos.CurrentRow.Cells["idgrupo"].Value.ToString();
            //txtNombre.Text = dgvGrupos.CurrentRow.Cells["nombre"].Value.ToString();
            //txtDescripcion.Text = dgvGrupos.CurrentRow.Cells["descripcion"].Value.ToString();
            
        }

        private void DgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarGrupo();
            BuscarGrupos("");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Exportar_Archivos ex = new Exportar_Archivos();
            ex.ExportarDataGridViewExcel(dgvGrupos);

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.DefaultPageSettings.Landscape = true;
            doc.PrinterSettings.PrinterName = "Microsoft Print to PDF";

            PrintPreviewDialog ppd = new PrintPreviewDialog { Document = doc };
            ((Form)ppd).WindowState = FormWindowState.Maximized;

            doc.PrintPage += delegate (object ev, PrintPageEventArgs ep)
            {
                const int DGV_ALTO = 35;
                int left = ep.MarginBounds.Left, top = ep.MarginBounds.Top;

                foreach (DataGridViewColumn col in dgvGrupos.Columns)
                {
                    ep.Graphics.DrawString(col.HeaderText, new System.Drawing.Font("Segoe UI", 16, FontStyle.Bold), Brushes.Pink, left, top);
                    left += col.Width;

                    if (col.Index < dgvGrupos.ColumnCount - 1)
                        ep.Graphics.DrawLine(Pens.Gray, left - 5, top, left - 5, top + 43 + (dgvGrupos.RowCount - 1) * DGV_ALTO);
                }
                left = ep.MarginBounds.Left;
                ep.Graphics.FillRectangle(Brushes.Black, left, top + 40, ep.MarginBounds.Right - left, 3);
                top += 43;

                foreach (DataGridViewRow row in dgvGrupos.Rows)
                {
                    if (row.Index == dgvGrupos.RowCount - 1) break;
                    left = ep.MarginBounds.Left;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        ep.Graphics.DrawString(Convert.ToString(cell.Value), new System.Drawing.Font("Segoe UI", 13), Brushes.Black, left, top + 4);
                        left += cell.OwningColumn.Width;
                    }
                    top += DGV_ALTO;
                    ep.Graphics.DrawLine(Pens.Gray, ep.MarginBounds.Left, top, ep.MarginBounds.Right, top);
                }
            };
            ppd.ShowDialog();


        }
    }
}
