﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Microsoft.Office.Interop.Excel;

namespace ControlEscolar2
{
    public partial class frmProfesor : Form
    {
        private ProfesorManejador _profesormanejador;
        private Profesores _profesor;

        private EstudioManejador _estudioManejador;
        private Estudios _estudio;
        public frmProfesor()
        {
            InitializeComponent();
            _profesormanejador = new ProfesorManejador();
            _profesor = new Profesores();

            _estudioManejador = new EstudioManejador();
            _estudio = new Estudios();
        }
        private void GuardarProfesor()
        {
            _profesormanejador.Guardar(_profesor);

        }
        public void getID(string filtro)
        {
            cmbID.DataSource = _profesormanejador.idprofesor(filtro);
            cmbID.DisplayMember = "Nextincremento";
            if(cmbID.Text == "")
            {
                lblID2.Text = "D" + cmbAño.Text + 1;
            }
            else
            {
                lblID2.Text = "D" + cmbAño.Text + cmbID.Text;
            }
           
            
        }
        private void CargarProfesor()
        {
            _profesor.ID = Convert.ToInt32(lblID.Text);
            
            _profesor.ncontrolm = lblID2.Text;
            _profesor.nombre = txtNombre.Text;
            _profesor.apellidopaterno = txtAp.Text;
            _profesor.apellidomaterno = txtAm.Text;
            _profesor.direccion = txtDir.Text;
            _profesor.ciudad = cmbCiudad.Text;
            _profesor.estado = cmbEstado.Text;
            _profesor.ncedula = txtCedula.Text;
            _profesor.titulo = txtTitulo.Text;
            _profesor.fechanacimiento = maskedTextBox1.Text;
        }

        private void FrmProfesor_Load(object sender, EventArgs e)
        {
            EstadoManejador _estadomanejador = new EstadoManejador();
            _estadomanejador.GetEstadosList(cmbEstado);
            BuscarProfesores("");
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDir.Enabled = activar;
            txtAp.Enabled = activar;
            txtAm.Enabled = activar;
           
            txtTitulo.Enabled = activar;
            maskedTextBox1.Enabled = activar;
            txtCedula.Enabled = activar;
            cmbEstado.Enabled = activar;
            cmbCiudad.Enabled = activar;
            dgvEstudio2.Enabled = activar;
        }
        private void Limpiar()
        {
          
            txtNombre.Clear();
            txtCedula.Clear();
            txtDir.Clear();
            txtBuscar.Clear();
            txtAp.Clear();
            txtAm.Clear();
            txtTitulo.Clear();

            lblID.Text = "0";
        }
        private void BuscarProfesores(string filtro)
        {
            dgvProfesor.DataSource = _profesormanejador.GetProfesores(filtro);
        }
        private void BuscarEstudios(string filtro)
        {
            dgvEstudio2.DataSource = _estudioManejador.GetEstudios2(filtro);
        }
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }
        private void ControlarTexto(bool uno)
        {
          
            txtAp.Enabled = uno;
            txtAm.Enabled = uno;
            txtNombre.Enabled = uno;
            txtDir.Enabled = uno;
            txtTitulo.Enabled = uno;
            txtCedula.Enabled = uno;
            maskedTextBox1.Enabled = uno;
            cmbEstado.Enabled = uno;
            cmbCiudad.Enabled = uno;


        }
        
       

        private void EliminarProfesor()
        {
            var ID = dgvProfesor.CurrentRow.Cells["ID"].Value;
            _profesormanejador.Eliminar(Convert.ToInt32(ID));
        }
        
       
        private void CmbCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CiudadesManejador _ciudadesmanejador = new CiudadesManejador();
            _ciudadesmanejador.GetCiudadesList(cmbEstado.Text, cmbCiudad);
        }
        
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtAm.Text = "";
            txtAp.Text = "";
            txtDir.Text = "";

            txtTitulo.Text = "";
          
            maskedTextBox1.Text = "";
            txtCedula.Text = "";
            lblID.Text = "0";
            cmbEstado.Text = "";
            cmbCiudad.Text = "";

        }

       

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProfesores(txtBuscar.Text);
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            dgvEstudio2.Visible = false;
            pictureBox1.Visible = true;
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarProfesor();
                    BuscarProfesores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificarProfesores()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblID.Text = dgvProfesor.CurrentRow.Cells["ID"].Value.ToString();
           
            txtNombre.Text = dgvProfesor.CurrentRow.Cells["nombre"].Value.ToString();
            txtAp.Text = dgvProfesor.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtAm.Text = dgvProfesor.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtDir.Text = dgvProfesor.CurrentRow.Cells["direccion"].Value.ToString();

            cmbEstado.Text = dgvProfesor.CurrentRow.Cells["estado"].Value.ToString();
            cmbCiudad.Text = dgvProfesor.CurrentRow.Cells["ciudad"].Value.ToString();
            txtCedula.Text = dgvProfesor.CurrentRow.Cells["ncedula"].Value.ToString();
            txtTitulo.Text = dgvProfesor.CurrentRow.Cells["titulo"].Value.ToString();
            maskedTextBox1.Text = dgvProfesor.CurrentRow.Cells["fechanacimiento"].Value.ToString();
        }

        private void DgvProfesor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarProfesores();
            BuscarProfesores("");
            dgvEstudio2.Visible = true;
            textBox1.Text = lblID.Text;
            pictureBox1.Visible = false;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarProfesor();
            if (true)
            {
                try
                {
                    GuardarProfesor();
                    Limpiar();
                    BuscarProfesores("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BtnAgregarEstudio_Click(object sender, EventArgs e)
        {
            frmEstudios estudio = new frmEstudios();
            estudio.ShowDialog();
        }

        private void DgvEstudio2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void LblID_Click(object sender, EventArgs e)
        {
            
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            
            BuscarEstudios(textBox1.Text);
        }

      
        private void TxtAm_TextChanged(object sender, EventArgs e)
        {

        }

        /*
        private void DtpInicio_ValueChanged(object sender, EventArgs e)
        {
            int ID = 0;
            int x = dtpInicio.Value.Year;

            DataSet ds;
            ds = _profesormanejador.LastID(x.ToString());
            if (ds.Tables[0].Rows.Count == 0)
            {
                ID++;
                txtNcontrol.Text = "D" + x.ToString() + ID.ToString();
                lblID.Text = ID.ToString();
            }
            else
            {
                string c = ds.Tables[0].Rows[0]["ncontrolm"].ToString();
                c = c.Substring(5);
                int v = int.Parse(c) + 1;

                txtNcontrol.Text = "D" + x.ToString() + v.ToString();
                lblID.Text = v.ToString();
            }
        }
        */

        private void DgvProfesor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvEstudio2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmEstudios est = new frmEstudios();
            
            est.ShowDialog();
        }

        private void CmbID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        private void CmbAño_SelectedIndexChanged(object sender, EventArgs e)
        {
            getID(cmbAño.Text);
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvProfesor);
        }

        private void ExportarDataGridViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo =
                        (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el datagrid rellenando la hoja de trabajo

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
            }
            catch (Exception)
            {
                MessageBox.Show("Fallo la creacion de archivo intenta de nuevo con otro nombre");
            
            }
        }
    }
}
