﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmAsignacionGrupo : Form

    {

        private GrupoManejador _grupomanejador;
        private Grupo _grupo;

        private AlumnoManejador _alumnomanejador;
        private Alumno _alumno;

        private AsGrupoManejador _asmanejador;
        private AsGrupo _asgrupo;
        public frmAsignacionGrupo()
        {
            InitializeComponent();

            _grupomanejador = new GrupoManejador();
            _grupo = new Grupo();

            _alumnomanejador = new AlumnoManejador();
            _alumno = new Alumno();

            _asmanejador = new AsGrupoManejador();
            _asgrupo = new AsGrupo();
        }

        private void FrmAsignacionGrupo_Load(object sender, EventArgs e)
        {
            //GrupoManejador _grupormanejador = new ProfesorManejador();
            _grupomanejador.GetGruposList(cmbGrupo);



            BuscarGrupos("");
            BuscarAlumnos("");
            ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            //Limpiar();

        }
        private void Limpiar()
        {
            lblIdalumno.Text = "";

            lblID.Text = "0";
            cmbGrupo.Text = "";



        }

        private void BuscarAlumnos(string filtro)
        {
            dgvAlumnos.DataSource = _alumnomanejador.GetAlumnos(filtro);
        }
        private void BuscarGrupos(string filtro)
        {
            dgvAsignaciones.DataSource = _asmanejador.GetAsig(filtro);
        }

        private void CargarGrupo()
        {
            _asgrupo.idasig = Convert.ToInt32(lblID.Text);
            _asgrupo.grupo = cmbGrupo.Text;
            _asgrupo.alumno = Convert.ToInt32(lblIdalumno.Text);
            _asgrupo.nombre = lblNombre.Text;


        }
        private void GuardarGrupo()
        {
            _asmanejador.Guardar(_asgrupo);

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarGrupo();
            if (true)
            {
                try
                {
                    GuardarGrupo();
                   // Limpiar();
                    BuscarGrupos("");
                    ControlarBotones(true, false, false, true);
                    //ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //BuscarGrupos(txtBuscar.Text);

            BuscarAlumnos(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarGrupo();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarGrupo()
        {
            var idasig = dgvAsignaciones.CurrentRow.Cells["idasig"].Value;
            _asmanejador.Eliminar(Convert.ToInt32(idasig));
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
      /*  private void ControlarCuadros(bool activar)
        {
            txtIdAlumno.Enabled = activar;

        }
        */
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            //ControlarCuadros(true);

            //txtIdAlumno.Focus();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            Limpiar();
        }

        private void ModificarGrupo()
        {
            //ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblID.Text = dgvAsignaciones.CurrentRow.Cells["idasig"].Value.ToString();
            lblIdalumno.Text = dgvAsignaciones.CurrentRow.Cells["Alumno"].Value.ToString();
           // txtIdAlumno.Enabled = false;
            
        }

       private void getid()
        {
            lblIdalumno.Text = dgvAlumnos.CurrentRow.Cells["idalumno"].Value.ToString();
            lblNombre.Text = dgvAlumnos.CurrentRow.Cells["nombre"].Value.ToString() + "  " + dgvAlumnos.CurrentRow.Cells["apellidopaterno"].Value.ToString() + "  " + dgvAlumnos.CurrentRow.Cells["apellidomaterno"].Value.ToString();
        }
        private void DgvAsignaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarGrupo();
            BuscarGrupos("");
        }

        private void DgvAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            getid();
        }

        private void CmbGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarGrupos("");
        }

        private void BtnAgregarEstudio_Click(object sender, EventArgs e)
        {
            frmGrupos gr = new frmGrupos();
            gr.ShowDialog();
        }
    }
}
