﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar2
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuario usuario = new frmUsuario();
            usuario.ShowDialog();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlumno alumno = new frmAlumno();
            alumno.ShowDialog();

        }

        private void ProfesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfesor profesor = new frmProfesor();
            profesor.ShowDialog();
        }

        private void EstudiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEstudios estudios = new frmEstudios();
            estudios.ShowDialog();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEscuela es = new FrmEscuela();
            es.ShowDialog();
        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMaterias mat = new frmMaterias();
            mat.ShowDialog();
        }

        private void GruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGrupos gru = new frmGrupos();
           gru.ShowDialog();
        }

        private void AsignacionesGruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAsignacionGrupo asig = new frmAsignacionGrupo();
            asig.ShowDialog();
        }

        private void AsignacionesProfesorYMateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReparto re = new frmReparto();
            re.ShowDialog();
        }

        private void CalificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCalificacion ca = new frmCalificacion();
            ca.ShowDialog();
        }
    }
}
