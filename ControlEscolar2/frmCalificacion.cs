﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Microsoft.Office.Interop.Excel;
using Exportar_PDF_Excel;
using System.Drawing.Printing;


namespace ControlEscolar2
{
    public partial class frmCalificacion : Form
    {
        private AsGrupoManejador _grupomanejador;
        private AsGrupo _grupo;

        private CalificacionManejador _calificacionmanejador;
        private Calificacion _calificacion;

        private AlumnoManejador _alumnomanejador;
        private Alumno _alumno;

        private GrupoManejador _grupossmanejador;
        private Grupo _grupos;

       // private RepartoManejador _repartomanejador;
       // private Reparto reparto;
      

       // private MateriaManejador _materiamanejador;
       // private Materia _materia;

        public frmCalificacion()
        {
            InitializeComponent();
            _grupomanejador = new AsGrupoManejador();
            _grupo = new AsGrupo();

            _calificacionmanejador = new CalificacionManejador();
            _calificacion = new Calificacion();
            _alumnomanejador = new AlumnoManejador();
            _alumno = new Alumno();
        }
        private void GuardarCalificacion()
        {
            _calificacionmanejador.Guardar(_calificacion);

        }
        private void CargarCalificacion()
        {
            _calificacion.idcalificacion = Convert.ToInt32(lblID.Text);
          
            _calificacion.alumno = cmbAlumnos.Text;
            _calificacion.grupo = cmbGrupo.Text;
            _calificacion.materia = cmbMaterias.Text;
            _calificacion.uno = Convert.ToInt32(txtUno.Text);
            _calificacion.dos = Convert.ToInt32(txtDos.Text);
            _calificacion.tres = Convert.ToInt32(txtTres.Text);
            _calificacion.cuatro = Convert.ToInt32(txtCuatro.Text);


        }
      /*  private void getDatos()
        {
            var ds = new DataSet();
            ds = _alumnomanejador.getNombre();
           

        }
        */
        private void FrmCalificacion_Load(object sender, EventArgs e)
        {


            //_grupossmanejador.GetGruposList(cmbGrupo);
            _grupomanejador.GetGruposList(cmbGrupo);
            BuscarGrupos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            Limpiar();
            // _repartoManejador.GetMateriasList(cmbGrupo.Text, cmbMaterias);
            //_repartomanejador.GetMateriasList(cmbGrupo.Text, cmbMaterias);

            // lblGrupo.Text = cmbGrupo.Text;

          

        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtUno.Enabled = activar;
            txtDos.Enabled = activar;
            txtTres.Enabled = activar;
            txtCuatro.Enabled = activar;


        }
        private void Limpiar()
        {

            txtUno.Clear();
            txtDos.Clear();
            txtTres.Clear();
            txtCuatro.Clear();


            lblID.Text = "0";
        }
        private void BuscarGrupos(string filtro)
        {
            dgvCalificacion.DataSource = _calificacionmanejador.GetCalificaciones(filtro);
        }
      /*  private void Buscaralumnos(string filtro)
        {
            dgvAl.DataSource = _alumnomanejador.GetAlumnos(filtro);
        }
        */
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtUno.Focus();
        }
        private void EliminarCalificacion()
        {
            var ID = dgvCalificacion.CurrentRow.Cells["idcalificacion"].Value;
            _calificacionmanejador.Eliminar(Convert.ToInt32(ID));
        }

        private void CmbAlumnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarGrupos(cmbAlumnos.Text);
            //_grupomanejador.GetAlumnosList(cmbGrupo.Text, cmbAlumnos);

            // _repartomanejador.GetMateriasList(cmbGrupo.Text, cmbMaterias);
            // _repartoManejador.GetMateriasList(cmbAlumnos.Text, cmbMaterias);
            //getDatos(cmbAlumnos.Text);
        }

        private void CmbGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarGrupos(cmbGrupo.Text);
            _grupomanejador.GetAlumnosList(cmbGrupo.Text, cmbAlumnos);

            _grupomanejador.GetMateriasList(cmbGrupo.Text, cmbMaterias);

            //_repartomanejador.GetMateriasList(cmbGrupo.Text, cmbMaterias);

           

            lblGrupo.Text = cmbGrupo.Text;
            // _repartoManejador.GetMateriasList(lblGrupo.Text, cmbMaterias);

           // var ds = new DataSet();
           // ds = _repartoManejador.getMateriasss(lblGrupo.Text);
           // cmbMaterias.Text = ds.Tables[0].Rows[0]["materia"].ToString();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            Limpiar();

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarCalificacion();
                    BuscarGrupos("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ModificarCalificaciones()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblID.Text = dgvCalificacion.CurrentRow.Cells["idcalificacion"].Value.ToString();

            txtUno.Text = dgvCalificacion.CurrentRow.Cells["uno"].Value.ToString();
            txtDos.Text = dgvCalificacion.CurrentRow.Cells["dos"].Value.ToString();
            txtTres.Text = dgvCalificacion.CurrentRow.Cells["tres"].Value.ToString();
            txtCuatro.Text = dgvCalificacion.CurrentRow.Cells["cuatro"].Value.ToString();




        }

        private void DgvCalificacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarCalificaciones();
            BuscarGrupos("");
           
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarCalificacion();
            if (true)
            {
                try
                {
                    GuardarCalificacion();
                    Limpiar();
                    BuscarGrupos("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
    }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Exportar_Archivos ex = new Exportar_Archivos();
            ex.ExportarDataGridViewExcel(dgvCalificacion);
        }

        private void BtnPdf_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.DefaultPageSettings.Landscape = true;
            doc.PrinterSettings.PrinterName = "Microsoft Print to PDF";

            PrintPreviewDialog ppd = new PrintPreviewDialog { Document = doc };
            ((Form)ppd).WindowState = FormWindowState.Maximized;

            doc.PrintPage += delegate (object ev, PrintPageEventArgs ep)
            {
                const int DGV_ALTO = 35;
                int left = ep.MarginBounds.Left, top = ep.MarginBounds.Top;

                foreach (DataGridViewColumn col in dgvCalificacion.Columns)
                {
                    ep.Graphics.DrawString(col.HeaderText, new System.Drawing.Font("Segoe UI", 16, FontStyle.Bold), Brushes.Pink, left, top);
                    left += col.Width;

                    if (col.Index < dgvCalificacion.ColumnCount - 1)
                        ep.Graphics.DrawLine(Pens.Gray, left - 5, top, left - 5, top + 43 + (dgvCalificacion.RowCount - 1) * DGV_ALTO);
                }
                left = ep.MarginBounds.Left;
                ep.Graphics.FillRectangle(Brushes.Black, left, top + 40, ep.MarginBounds.Right - left, 3);
                top += 43;

                foreach (DataGridViewRow row in dgvCalificacion.Rows)
                {
                    if (row.Index == dgvCalificacion.RowCount - 1) break;
                    left = ep.MarginBounds.Left;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        ep.Graphics.DrawString(Convert.ToString(cell.Value), new System.Drawing.Font("Segoe UI", 13), Brushes.Black, left, top + 4);
                        left += cell.OwningColumn.Width;
                    }
                    top += DGV_ALTO;
                    ep.Graphics.DrawLine(Pens.Gray, ep.MarginBounds.Left, top, ep.MarginBounds.Right, top);
                }
            };
            ppd.ShowDialog();
        }

        private void LblNombre_Click(object sender, EventArgs e)
        {
            //getDatos();
        }
    }
}


