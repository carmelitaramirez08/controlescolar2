﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;

using LogicaNegocios.ControlEscolar2;
using System.IO;
using System.Drawing.Imaging;
using MySql.Data;

using MySql.Data.MySqlClient;
namespace ControlEscolar2
{
    public partial class FrmEscuela : Form
    {
        private EscuelaManejador _escuelamanejador;
        private Escuela _escuela;

        //JPG
        private OpenFileDialog _imagenJpg;
        private string _rutaL;

        public FrmEscuela()
        {
            InitializeComponent();
            _escuelamanejador = new EscuelaManejador();
            _escuela = new Escuela();

            //JPG
            _imagenJpg = new OpenFileDialog();
            _rutaL = Application.StartupPath + "\\LOGO\\";
        }


        private void FrmEscuela_Load(object sender, EventArgs e)
        {
            ControlarBotones(false, false);
            ControlarCuadros(true);
            Limpiar();
            BuscarEscuela("");

            // string _imagenJpg = @"C:\Users\Ramirez\Desktop\ControlEscolar2\ControlEscolar2\bin\Debug\_imagenJpg";
            //imgLogo.Image = Image.FromFile(_imagenJpg);


            //imgLogo.Image = new Bitmap(new MemoryStream());
            getDatos();

            dgvEscuela.Visible = false;

            groupBox1.Enabled = false;


            btnCancelar.Enabled = false;
            btnGuardar.Enabled = false;



        }
        private void BuscarProfesores(string filtro)
        {
            dgvEscuela.DataSource = _escuelamanejador.GetEscuela(filtro);
        }

        private void GuardarEscuela()
        {
            _escuelamanejador.Guardar(_escuela);

        }
        private void CargarEscuela()
        {
            _escuela.idescuela = Convert.ToInt32(lblID.Text);
            _escuela.nombre = txtNombre.Text;
            _escuela.rfc = txtRfc.Text;
            _escuela.domicilio = txtDomicilio.Text;
            _escuela.telefono = txtTelefono.Text;
            _escuela.email = txtEmail.Text;
            _escuela.paginaweb = txtPagina.Text;
            _escuela.director = txtDirector.Text;
            _escuela.logo = txtLogo.Text;

        }
        private void ControlarBotones( bool guardar, bool cancelar)
        {
           
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtRfc.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtTelefono.Enabled = activar;

            txtEmail.Enabled = activar;
            txtPagina.Enabled = activar;
            txtDirector.Enabled = activar;
            txtLogo.Enabled = activar;
            
        }
        private void Limpiar()
        {

            txtNombre.Clear();
            txtRfc.Clear();
            txtDomicilio.Clear();
            txtTelefono.Clear();

            txtEmail.Clear();
            txtPagina.Clear();
            txtDirector.Clear();
            txtLogo.Clear();

            lblID.Text = "0";
        }
        private void BuscarEscuela(string filtro)
        {
            dgvEscuela.DataSource = _escuelamanejador.GetEscuela(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones( true, true);
            ControlarCuadros(true);

            txtNombre.Focus();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEscuela(txtBuscar.Text);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarEscuela();
            if (true)
            {
                try
                {
                    GuardarEscuela();
                   
                    BuscarProfesores("");
                    ControlarBotones(true, true);
                    ControlarCuadros(true);
                    MessageBox.Show("Datos actualizados");
                    
                    GuardarImagenJpg();
                    if (txtLogo.Text == "")
                    {
                        EliminarImagenJpg();
                        Imagen();

                    }
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

            groupBox1.Enabled = false;
            btnCancelar.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void DgvEscuela_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarEscuela();
            BuscarEscuela("");
         
         
          
        }
        private void datos()
        {
        

                //ds.Tables(0).Rows(0)(1)
        }
        private void ModificarEscuela()
        {
            ControlarCuadros(true);
            ControlarBotones( true, true);
           // lblID.Text = dgvEscuela.CurrentRow.Cells["idescuela"].Value.ToString();

            /* txtNombre.Text = dgvEscuela.CurrentRow.Cells["nombre"].Value.ToString();
             txtRfc.Text = dgvEscuela.CurrentRow.Cells["rfc"].Value.ToString();
             txtDomicilio.Text = dgvEscuela.CurrentRow.Cells["domicilio"].Value.ToString();
             txtTelefono.Text = dgvEscuela.CurrentRow.Cells["telefono"].Value.ToString();
             txtEmail.Text = dgvEscuela.CurrentRow.Cells["email"].Value.ToString();
             txtPagina.Text = dgvEscuela.CurrentRow.Cells["paginaweb"].Value.ToString();
             txtDirector.Text = dgvEscuela.CurrentRow.Cells["director"].Value.ToString();
             txtLogo.Text = dgvEscuela.CurrentRow.Cells["logo"].Value.ToString();*/

            
           


            Imagen();



            //imgLogo.Image = Image.FromFile();

            /* string Ruta = @"C:\Users\Ramirez\Desktop\ControlEscolar2\ControlEscolar2\bin\Debug\img";

             if (File.Exists(Ruta))
             {
                 FileStream fs = new System.IO.FileStream(Ruta, FileMode.Open, FileAccess.Read);
                 imgLogo.Image = Image.FromStream(fs);
                 fs.Close();
             }


     */


            txtRuta.Text = _rutaL;


        }
        private void getDatos()
        { 
        var ds= new DataSet();
            ds = _escuelamanejador.getDatos();
            lblID.Text = ds.Tables[0].Rows[0]["idescuela"].ToString();
            txtNombre.Text = ds.Tables[0].Rows[0]["nombre"].ToString();
            txtRfc.Text = ds.Tables[0].Rows[0]["rfc"].ToString();
            txtDomicilio.Text = ds.Tables[0].Rows[0]["domicilio"].ToString();
            txtTelefono.Text = ds.Tables[0].Rows[0]["telefono"].ToString();
            txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
            txtPagina.Text = ds.Tables[0].Rows[0]["paginaweb"].ToString();
            txtDirector.Text = ds.Tables[0].Rows[0]["director"].ToString();
            txtLogo.Text = ds.Tables[0].Rows[0]["logo"].ToString();


            txtRuta.Text = _rutaL;
            Imagen();
            ControlarCuadros(true);
            ControlarBotones(true, true);
        }
    private void Imagen()
        {
            this.imgLogo.ImageLocation = _rutaL + txtLogo.Text;
            imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, false);
            ControlarCuadros(false);
            Limpiar();
            Imagen();
            getDatos();
            groupBox1.Enabled = false;
                
            
            
        }

        
private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarImagen();
        }

        private void CargarImagen()
        {
            _imagenJpg.Filter = "Imagen tipo (*.jpg)|*.jpg";
            _imagenJpg.Title = "Cargar Imagen";
            _imagenJpg.ShowDialog();

        
            if (_imagenJpg.FileName != "")
                
            {
                var archivo = new FileInfo(_imagenJpg.FileName);
               
                txtLogo.Text = archivo.Name;
                if (archivo.Length > 2000000)
                {
                    MessageBox.Show("Imagen demasiado grande, intente con otra :(");
                    txtLogo.Clear();
                    getDatos();
                }
                else { 
                imgLogo.ImageLocation = _imagenJpg.FileName;

                //string imagen = _imagenJpg.FileName;

                imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;

                    //this.imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;




                }
            }


        }

        private void GuardarImagenJpg()
        {
            
            
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivo = new FileInfo(_imagenJpg.FileName);
                   
                  
                    if (Directory.Exists(_rutaL))
                    {
                        //Codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_rutaL, "*.jpg");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            //Codigo para reemplazar imagen

                            archivoAnterior = new FileInfo(obtenerArchivos[0]);

                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_rutaL + archivo.Name);
                            }
                        }
                        else
                        {
                            try
                            {
                                archivo.CopyTo(_rutaL + archivo.Name);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);

                            }

                        }


                    }
                    else

                    {
                        Directory.CreateDirectory(_rutaL);
                        archivo.CopyTo(_rutaL + archivo.Name);
                    }
                }
            }
        }

            private void BtnSubir_Click(object sender, EventArgs e)
        {
            GuardarImagenJpg();
            //pictureBox1.Image = new System.Drawing.Bitmap(@"\img\.jpg");
        }


        private void EliminarImagenJpg()
        {
            if (MessageBox.Show("Estas seguro de eliminar la imagen", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)

                if (Directory.Exists(_rutaL))
                {
                    //Codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_rutaL, "*.jpg");
                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {
                        //Codigo para reemplazar imagen

                        archivoAnterior = new FileInfo(obtenerArchivos[0]);

                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }

                    }
                }

        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            //EliminarImagenJpg();
            ModificarEscuela();
            txtLogo.Text = "";
            
            


        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
                
            }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            btnCancelar.Enabled = true;
            btnGuardar.Enabled = true;
        }
    }

        
    }


