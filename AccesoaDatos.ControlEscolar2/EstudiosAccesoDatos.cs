﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class EstudiosAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstudiosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Estudios estudio)
        {
            if (estudio.idestudio == 0)
            {
                string consulta = string.Format("Insert into estudios values(null,'{0}','{1}','{2}','{3}')",
                    estudio.estudio, estudio.anioestudio, estudio.documento, estudio.fk_profesor);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update estudios set estudio ='{0}', anioestudio= '{1}', documento= '{2}', fk_profesor='{3}' where idestudio= {4}",
                 estudio.estudio, estudio.anioestudio, estudio.documento, estudio.fk_profesor, estudio.idestudio);
                 conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idestudio)
        {
            string consulta = string.Format("Delete from estudios where idestudio={0}", idestudio);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Estudios> GetEstudios(string filtro)
        {

            var listEstudio = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where idestudio like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estudio = new Estudios
                {
                    idestudio = Convert.ToInt32(row["idestudio"]),
                    estudio = row["estudio"].ToString(),
                    anioestudio = Convert.ToInt32(row["anioestudio"]),
                    documento = row["documento"].ToString(),
                    fk_profesor = Convert.ToInt32(row["fk_profesor"].ToString()),
                    
                   

                };

                listEstudio.Add(estudio);
            }

            return listEstudio;
        }

        //GridEstudios

        public List<Estudios> GetEstudios2(string filtro)
        {

            var listEstudio = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where fk_profesor like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estudio = new Estudios
                {
                    idestudio = Convert.ToInt32(row["idestudio"]),
                    estudio = row["estudio"].ToString(),
                    anioestudio = Convert.ToInt32(row["anioestudio"]),
                    documento = row["documento"].ToString(),
                    fk_profesor = Convert.ToInt32(row["fk_profesor"].ToString()),



                };

                listEstudio.Add(estudio);
            }

            return listEstudio;
        }



    }
}
