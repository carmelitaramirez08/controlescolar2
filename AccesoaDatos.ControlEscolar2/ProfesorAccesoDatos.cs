﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class ProfesorAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public ProfesorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Profesores profesor)
        {
            //if (Convert.ToInt32(profesor.ncontrolm) == 0)
            if (profesor.ID == 0)
            {
               string consulta = string.Format("Insert into profesores values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", 
                   profesor.ncontrolm, profesor.nombre, profesor.apellidopaterno, profesor.apellidomaterno, profesor.direccion,profesor.ciudad, profesor.estado, profesor.ncedula, profesor.titulo, profesor.fechanacimiento);
                conexion.EjecutarConsulta(consulta);

               //string consulta = "Insert into profesores values('"+ profesor.ncontrolm + "','" + profesor.nombre + "','" + profesor.apellidopaterno + "','" + profesor.apellidomaterno + "','" + profesor.direccion + "','" + profesor.ciudad + "','" + profesor.estado + "','" + profesor.ncedula + "','" + profesor.titulo + "','" + profesor.fechanacimiento + "')";
                //conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = "Update  profesores set ncontrolm='"+profesor.ncontrolm+"', nombre='" + profesor.nombre + "',apellidopaterno='" +
                    profesor.apellidopaterno + "',apellidomaterno='" + profesor.apellidomaterno + "',direccion='" + profesor.direccion + "',ciudad='" + profesor.ciudad + "',estado='" + profesor.estado + "', ncedula='" + profesor.ncedula + "', titulo='" + profesor.titulo + "', fechanacimiento='" + profesor.fechanacimiento+ "' where ID=" + profesor.ID + "";
                conexion.EjecutarConsulta(consulta);
            }
        }

       /*public DataSet LastID(string filtro)
        {
            var ds = new DataSet();

            string consulta = "select ncontrolm max(substring(ncontrolm,6)) as Nextincremento from profesores where ncontrolm like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            return ds;
        }
        */
        public DataTable idprofesor(string fecha)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select max(substring(ncontrolm,6)+1) as Nextincremento from profesores where ncontrolm like '%" + fecha + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            dt = ds.Tables[0];
            return dt;
        }

        public void Eliminar(int ID)
        {
            //eliminar
            string consulta = "delete from profesores where ID=" +ID + "";
            conexion.EjecutarConsulta(consulta);

        }

        public List<Profesores> GetProfesores(string filtro)
        {
            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "select * from profesores where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var profesor = new Profesores
                {
                    ID = Convert.ToInt32(row["ID"]),
                
                    ncontrolm = row["ncontrolm"].ToString(),
                    nombre = row["nombre"].ToString(),
                    apellidopaterno = row["apellidopaterno"].ToString(),
                    apellidomaterno = row["apellidomaterno"].ToString(),
                    direccion = row["direccion"].ToString(),
                    ciudad = row["ciudad"].ToString(),
                    estado = row["estado"].ToString(),
                    ncedula = row["ncedula"].ToString(),
                    titulo = row["titulo"].ToString(),
                    fechanacimiento = row["fechanacimiento"].ToString()
                    

                };
                listProfesores.Add(profesor);
            }
            //Llenar lista
            return listProfesores;
        }

        public List<Profesores> GetProfesoresList(ComboBox cm)

        {
            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "select ID from profesores";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "ID";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Profesores
                {
                    ncontrolm = row["ID"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }

        //numero de control para reparto
        public List<Profesores> GetProfesoresNumeroControl(ComboBox cm)

        {
            var listProfesores = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "select ncontrolm from profesores";
            ds = conexion.ObtenerDatos(consulta, "profesores");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "ncontrolm";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Profesores
                {
                    ncontrolm = row["ncontrolm"].ToString()
                };
                listProfesores.Add(profesores);
            }
            return listProfesores;
        }

        public List<Profesores> GetID(string filtro)
        {
            var listProfesor = new List<Profesores>();
            var ds = new DataSet();
            string consulta = "Select ID from profesores where nombre  like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "profesores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesores = new Profesores
                {
                    ID = Convert.ToInt32(row["ID"].ToString())
                };

                listProfesor.Add(profesores);
            }

            return listProfesor;
        }


    }
}
