﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Grupo grupo)
        {
           
            if (grupo.idgrupo == 0)
            {
                string consulta = string.Format("Insert into grupos values(null,'{0}')",
                    grupo.nombre);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                
                string consulta = "Update  grupos set nombre='" + grupo.nombre +  "' where idgrupo=" + grupo.idgrupo + "";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar(int idgrupo)
        {
            //eliminar
            string consulta = "delete from grupos where idgrupo=" + idgrupo + "";
            conexion.EjecutarConsulta(consulta);

        }

        public DataTable grupos(string fecha)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select * from grupos";
            ds = conexion.ObtenerDatos(consulta, "grupos");
            dt = ds.Tables[0];
            return dt;
        }

        public List<Grupo> GetGrupos(string filtro)
            
        {
            var listGrupos = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "select * from grupos where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var grupos = new Grupo
                {
                    idgrupo = Convert.ToInt32(row["idgrupo"]),
                    nombre = row["nombre"].ToString(),
              


                };
                listGrupos.Add(grupos);
            }
            //Llenar lista
            return listGrupos;
        }
        

        public List<Grupo> GetGruposList(ComboBox cm)

        {
            var listGrupos = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "select nombre from grupos";
            ds = conexion.ObtenerDatos(consulta, "grupos");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupos = new Grupo
                {
                    nombre = row["nombre"].ToString()
                };
                listGrupos.Add(grupos);
            }
            return listGrupos;
        }


    }
}
