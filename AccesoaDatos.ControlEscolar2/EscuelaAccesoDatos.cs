﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Escuela escuela)
        {
            if (escuela.idescuela == 0)
            {
                string consulta = string.Format("Insert into escuela values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                escuela.idescuela, escuela.nombre, escuela.rfc, escuela.domicilio, escuela.telefono, escuela.email, escuela.paginaweb, escuela.director, escuela.logo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {





                string consulta = "Update  escuela set nombre='" + escuela.nombre + "', rfc='" + escuela.rfc + "',domicilio='" +
                    escuela.domicilio + "',telefono='" + escuela.telefono + "',email='" + escuela.email + "',paginaweb='" + escuela.paginaweb + "',director='" + escuela.director + "', logo='" + escuela.logo + "' where idescuela=" + escuela.idescuela + "";
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idescuela)
        {
            string consulta = string.Format("Delete from escuela where idescuela={0}", idescuela);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Escuela> GetEscuelas(string filtro)
        {

            var listEscuela = new List<Escuela>();
            var ds = new DataSet();
            string consulta = "Select * from escuela where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    idescuela = Convert.ToInt32(row["idescuela"]),
                    nombre = row["nombre"].ToString(),
                    rfc = row["rfc"].ToString(),
                    domicilio = row["domicilio"].ToString(),
                    telefono = row["telefono"].ToString(),
                    email = row["email"].ToString(),
                    paginaweb = row["nombre"].ToString(),
                    director = row["director"].ToString(),
                    logo = row["logo"].ToString(),

                };

                listEscuela.Add(escuela);
            }

            return listEscuela;
        }


        public DataSet getDatos()
        {
            var ds = new DataSet();
        string consulta = "Select * from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;
    
         }

}
}

