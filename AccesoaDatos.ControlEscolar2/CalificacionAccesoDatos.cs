﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;
namespace AccesoaDatos.ControlEscolar2
{
   public class CalificacionAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CalificacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Calificacion calificacion)
        {
           
            if (calificacion.idcalificacion == 0)
            {
                string consulta = string.Format("Insert into calificaciones values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                    calificacion.alumno, calificacion.grupo, calificacion.materia, calificacion.uno, calificacion.dos, calificacion.tres,calificacion.cuatro);
                conexion.EjecutarConsulta(consulta);

                
            }
            else
            {
                //update
                string consulta = "Update  calificaciones set alumno='" + calificacion.alumno + "', grupo='" + calificacion.grupo + "',materia='" +
                    calificacion.materia + "',uno='" + calificacion.uno + "',dos='" + calificacion.dos + "',tres='" + calificacion.tres +
                    "',cuatro='" + calificacion.cuatro + "' where idcalificacion=" + calificacion.idcalificacion + "";
                conexion.EjecutarConsulta(consulta);

               
                conexion.EjecutarConsulta(consulta);
            }
        }

        public void Eliminar(int idcalificacion)
        {
            //eliminar
            string consulta = "delete from calificaciones where idcalificacion=" + idcalificacion + "";
            conexion.EjecutarConsulta(consulta);

        }

        public List<Calificacion> GetCalificaciones(string filtro)
        {
            var listCalificaciones = new List<Calificacion>();
            var ds = new DataSet();
            string consulta = "select * from calificaciones where alumno like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "calificaciones");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var calificacion = new Calificacion
                {
                    idcalificacion = Convert.ToInt32(row["idcalificacion"]),

                    alumno = row["alumno"].ToString(),
                   
                    grupo = row["grupo"].ToString(),
                    materia= row["materia"].ToString(),

                    uno = Convert.ToInt32(row["uno"]),

                    dos = Convert.ToInt32(row["dos"]),

                   tres = Convert.ToInt32(row["tres"]),

                    cuatro = Convert.ToInt32(row["cuatro"]),


                };
                listCalificaciones.Add(calificacion);
            }
            //Llenar lista
            return listCalificaciones;
        }



    }
}
