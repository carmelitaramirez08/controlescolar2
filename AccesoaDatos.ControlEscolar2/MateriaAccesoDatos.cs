﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;


namespace AccesoaDatos.ControlEscolar2
{
    
    public class MateriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MateriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Materia materia)
        {
            if (materia.idmateria == 0)
            {
                string consulta = string.Format("Insert into materias values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                materia.clave, materia.nombre, materia.horast, materia.horasp, materia.creditos, materia.semestre, materia.anterior, materia.siguiente);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = "Update  materias set clave='" + materia.clave + "', nombre='" +materia.nombre + "',horast='" +
                    materia.horast + "',horasp='" + materia.horasp + "',creditos='" + materia.creditos + "',semestre='" + materia.semestre + "',anterior='" + materia.anterior+ "', siguiente='" + materia.siguiente +"' where ID=" + materia.idmateria + "";
                conexion.EjecutarConsulta(consulta);
            }

        }

        public DataTable idmateria(string clave)
        {
            var dt = new DataTable();
            var ds = new DataSet();
            string consulta = "select max(substring(clave,6)+1) as Nextincremento from materias where clave like '%" + clave + "%'";
            ds = conexion.ObtenerDatos(consulta, "materias");
            dt = ds.Tables[0];
            return dt;
        }

        public void Eliminar(int idmateria)
        {
            //eliminar
            string consulta = "delete from materias where idmateria=" + idmateria + "";
            conexion.EjecutarConsulta(consulta);

        }

        public List<Materia> GetMaterias(string filtro)
        {
            var listMaterias = new List<Materia>();
            var ds = new DataSet();
            string consulta = "select * from materias where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materias");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var materia = new Materia
                {
                    idmateria = Convert.ToInt32(row["idmateria"]),

                    clave = row["clave"].ToString(),
                    nombre = row["nombre"].ToString(),
                    horast = Convert.ToInt32(row["horast"]),
                    horasp = Convert.ToInt32(row["horasp"]),
                    creditos = Convert.ToInt32(row["creditos"]),
                    anterior = row["anterior"].ToString(),
                    siguiente = row["siguiente"].ToString(),


                };
                listMaterias.Add(materia);
            }
            //Llenar lista
            return listMaterias;
        }



        public List<Materia> GetMateriasList(ComboBox cm)

        {
            var listMaterias = new List<Materia>();
            var ds = new DataSet();
            string consulta = "select nombre from materias";
            ds = conexion.ObtenerDatos(consulta, "materias");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materias = new Materia
                {
                    nombre = row["nombre"].ToString()
                };
                listMaterias.Add(materias);
            }
            return listMaterias;
        }


    }
}
