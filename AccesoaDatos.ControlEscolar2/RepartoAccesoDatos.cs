﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class RepartoAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public RepartoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Reparto reparto)
        {
           
            if (reparto.idreparto == 0)
            {
                string consulta = string.Format("Insert into reparto values(null,'{0}','{1}','{2}')",
                   reparto.grupo, reparto.fkprofesor, reparto.materia);
                conexion.EjecutarConsulta(consulta);

               
            }
            else
            {
                //update
                string consulta = "Update  reparto set grupo='" + reparto.grupo + "', fkprofesor='" + reparto.fkprofesor + "',materia='" +
                    reparto.materia + "' where idreparto=" + reparto.idreparto+ "";
                conexion.EjecutarConsulta(consulta);
            }
        }


        public void Eliminar(int idreparto)
        {
            //eliminar
            string consulta = "delete from reparto where idreparto=" + idreparto + "";
            conexion.EjecutarConsulta(consulta);

        }

        public List<Reparto> GetRepartos(string filtro)
        {
            var listRepartos = new List<Reparto>();
            var ds = new DataSet();
            string consulta = "select * from reparto where grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "reparto");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var reparto = new Reparto
                {
                    idreparto = Convert.ToInt32(row["idreparto"]),

                    grupo= row["grupo"].ToString(),
                    materia = row["materia"].ToString(),
                    fkprofesor = row["fkprofesor"].ToString(),
                 


                };
                listRepartos.Add(reparto);
            }
            //Llenar lista
            return listRepartos;
        }

        public List<Reparto> GetMateriasList(string grupo, ComboBox cm)
        {
            var listMaterias = new List<Reparto>();
            var ds = new DataSet();
            string consulta = "select materia from reparto where grupo = '" + grupo+ "'";
            ds = conexion.ObtenerDatos(consulta, "reparto");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "materia";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var reparto = new Reparto
                {
                    materia = row["materia"].ToString()

                };
                listMaterias.Add(reparto);
            }
            return listMaterias;




        }
        public DataSet getMateriasss(string grupo)
        {
            var ds = new DataSet();
            string consulta = "Select materia from escuela where grupo ='" + grupo + "'";
            ds = conexion.ObtenerDatos(consulta, "reparto");
            return ds;

        }
    }
}
