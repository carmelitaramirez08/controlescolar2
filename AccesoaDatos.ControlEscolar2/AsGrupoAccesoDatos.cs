﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos.ControlEscolar2
{
    public class AsGrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AsGrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(AsGrupo asGrupo)
        {
            if (asGrupo.idasig == 0)
            {
                string consulta = string.Format("Insert into agrupo values(null,'{0}','{1}','{2}')",
                   asGrupo.grupo, asGrupo.alumno, asGrupo.nombre);
                conexion.EjecutarConsulta(consulta);

                /* string consulta = string.Format("Insert into profesores values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", 
                   profesor.ncontrolm, profesor.nombre, profesor.apellidopaterno, profesor.apellidomaterno, profesor.direccion,profesor.ciudad, profesor.estado, profesor.ncedula, profesor.titulo, profesor.fechanacimiento);
                conexion.EjecutarConsulta(consulta);*/
            }
            else
            {
                
                /* string consulta = string.Format("Update agrupo set grupo ='{0}',  where idasig= {1}",
                  asGrupo.grupo, asGrupo.idasig);
                 conexion.EjecutarConsulta(consulta);*/
                string consulta = string.Format("Update agrupo set grupo ='{0}', alumno= '{1}' , nombre= '{2}' where idasig= {3}",
                 asGrupo.grupo, asGrupo.alumno, asGrupo.nombre, asGrupo.idasig);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idasig)
        {
            //eliminar
            string consulta = "delete from agrupo where idasig=" + idasig + "";
            conexion.EjecutarConsulta(consulta);

        }
        public List<AsGrupo> GetGrupos(string filtro)
        {
            var listGrupos = new List<AsGrupo>();
            var ds = new DataSet();
            string consulta = "select * from agrupo where grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "agrupo");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var asgrupo = new AsGrupo
                {
                     idasig = Convert.ToInt32(row["idasig"]),
                    grupo = row["grupo"].ToString(),

                    alumno = Convert.ToInt32(row["alumno"]),
                    nombre = row["nombre"].ToString(),




                };
                listGrupos.Add(asgrupo);
            }
            //Llenar lista
            return listGrupos;
        }


        public List<AsGrupo> GetGruposList(ComboBox cm)

        {
            var listGrupos = new List<AsGrupo>();
            var ds = new DataSet();
            string consulta = "select grupo from agrupo";
            ds = conexion.ObtenerDatos(consulta, "agrupo");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "grupo";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupos = new AsGrupo
                {
                    grupo = row["grupo"].ToString()
                };
                listGrupos.Add(grupos);
            }
            return listGrupos;
        }



        public List<AsGrupo> GetAlumnosList(string grupo, ComboBox cm)
        {
            var listAlumnos = new List<AsGrupo>();
            var ds = new DataSet();
            string consulta = "select alumno from agrupo where grupo = '" + grupo+ "'";
            ds = conexion.ObtenerDatos(consulta, "agrupo");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "alumno";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asgrupo = new AsGrupo
                {
                    alumno = Convert.ToInt32(row["alumno"].ToString())

                };
                listAlumnos.Add(asgrupo);
            }
            return listAlumnos;

        }

      
        
    }
}

